package TravailP;

/**
 * SandwichGourments class is a representation of a table where three persons eat;
 * The Baker, the Butcher and the Milkman. In order to make their own sandwich and eat they
 * will need to two other ingredient from the rest. Those items are shared variables.
 * Some more comments
 * 
 * @author      Roberto Ramos Chavez <roberto.ramoschavez@edu.hefr.ch>
 * @version     1.9                  
 * @since       2012-04-20
 *
 * Completion time: 7hrs :
 *
 * Honor Code: I pledge that this program represents my own
 *   program code. I received help from the little book of Semaphores.
 *
 */

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SandwichGourmets {
	

	//Will allow access to stop or continue giving the ingredients to the three workers
    public static Semaphore grocer_semaphore  = new Semaphore(1, false);
    
    public static Semaphore bread             = new Semaphore(0, true);
    public static Semaphore ham               = new Semaphore(0, true);
    public static Semaphore butter            = new Semaphore(0, true);
    
    public static Semaphore mutexBread        = new Semaphore(0, true);
    public static Semaphore mutexHam      	  = new Semaphore(0, true);
    public static Semaphore mutexButter       = new Semaphore(0, true);
    
    //Mutex exclusion while enter each of the critical section in each ingredient
    public static Semaphore mutex             = new Semaphore(1, true);
    
    //This boolean will indicate if the ingredient is on the table
    public static boolean isBread, isHam, isButter=false;

    public static ExecutorService threadExecutor = Executors
            .newCachedThreadPool();

    public static void main(String[] args) {
        Grocer grocer   = new Grocer();
        Baker baker     = new Baker();
        Butcher butcher = new Butcher();
        Milkman milkman = new Milkman();
        PushBa pusherBaker= new PushBa();
        PushBu pusherButcher=new PushBu();
        PushMi pusherMilkman=new PushMi();
        threadExecutor.execute(grocer);
        threadExecutor.execute(baker);
        threadExecutor.execute(butcher);
        threadExecutor.execute(milkman);
		threadExecutor.execute(pusherBaker);
        threadExecutor.execute(pusherButcher);
        threadExecutor.execute(pusherMilkman);
    }

}