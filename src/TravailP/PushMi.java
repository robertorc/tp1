package TravailP;
/**
 * This class will be managing in the interactions form Milkman with the Grocer 
 * The PushBa class for the Sandwich Gourmet Problem. This class will manage when 
 * the ingredients need are on the table and when are not, so while to not retain any 
 * ingredient in order to let the others eat and eliminate deadlock.
 * 
 * Concurrent Programming - TP6
 * 
 * @author      Roberto Ramos Chavez <roberto.ramoschavez@edu.hefr.ch>
 * @version     1.9               
 * @since       2012-04-20
 *
 * 
 *
 */
public class PushMi implements Runnable {
	
	public void run(){
		while(true){
			try {
				SandwichGourmets.butter.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//Mutual exclusion for the critical region
			try {
				SandwichGourmets.mutex.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//Testing for any Bread on the table
			if(SandwichGourmets.isBread){
				SandwichGourmets.isBread=false;
				SandwichGourmets.mutexHam.release();
			}
			else if(SandwichGourmets.isHam){
				SandwichGourmets.isHam=false;
				SandwichGourmets.mutexBread.release();
			}
			else 
				SandwichGourmets.isButter=true;
				SandwichGourmets.mutex.release();
			
		}
	}
}
