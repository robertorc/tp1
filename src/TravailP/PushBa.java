package TravailP;


/**
 * This class will be managing in the interactions from the Baker with the Grocer. 
 * PushBu=PushButcher will interact with the table where the Butcher, the Baker and
 * the Milker eat.This class will manage when the ingredients need are on the table
 * and when are not, so while to not retain any ingredient in order to let the others
 * eat and eliminate deadlock.
 *  
 * The PushBa class for the Sandwich Gourmet Problem
 * Concurrent Programming - TP6
 * 
 * @author      Roberto Ramos Chavez <roberto.ramoschavez@edu.hefr.ch>
 * @version     1.9               
 * @since       2012-04-20
 *
 * 
 *
 */

public class PushBa implements Runnable{

	public void run() {
		while(true){
			try {
				SandwichGourmets.bread.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//Mutual exclusion for the critical region
			try {
				SandwichGourmets.mutex.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//Testing for any Bread on the table
			if(SandwichGourmets.isHam){
				SandwichGourmets.isHam=false;
				SandwichGourmets.mutexButter.release();
			}
			else if(SandwichGourmets.isButter){
				SandwichGourmets.isButter=false;
				SandwichGourmets.mutexHam.release();
			}
			else 
				SandwichGourmets.isBread=true;
				SandwichGourmets.mutex.release();
			
		}
	}

}
