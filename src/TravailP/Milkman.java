package TravailP;

/**
 * 
 * The Milkman class for the Sandwich Gourmet Problem. 
 * Concurrent Programming - TP6
 * 
 * @author      Roberto Ramos Chavez <roberto.ramoschavez@edu.hefr.ch>
 * @version     1.9                
 * @since       2012-04-20
 *
 * 
 *
 */

public class Milkman implements Runnable {

 @Override
 public void run() {
     while (true) {
    	 // wait for bread and ham
    	 try {
			SandwichGourmets.mutexButter.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 	
    	 System.out.println("Milkman adds butter and makes sandwich");
    	 
    	 //Clean the table after eating
    	 SandwichGourmets.isBread	= false;
    	 SandwichGourmets.isButter	= false;
    	 SandwichGourmets.isHam		= false;
    	 
    	 SandwichGourmets.grocer_semaphore.release();
    	 System.out.println("Milkman eats his sandwich");
     }
 }
}
