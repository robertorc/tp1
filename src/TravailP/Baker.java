package TravailP;

/**
 * 
 * The Baker class for the Sandwich Gourmet Problem
 * Concurrent Programming - TP6
 * 
 * @author      Roberto Ramos Chavez <roberto.ramoschavez@edu.hefr.ch>
 * @version     1.9             
 * @since       2012-04-20
 *
 * 
 *
 */

public class Baker implements Runnable {

 @Override
 public void run() {
     while (true) {
         // wait for ham and butter
    	 try {
			SandwichGourmets.mutexBread.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 		
    	 System.out.println("Baker adds bread and makes sandwich");
    	//Clean the table after eating
    	 SandwichGourmets.isBread	= false;
    	 SandwichGourmets.isButter	= false;
    	 SandwichGourmets.isHam		= false;
    	 SandwichGourmets.grocer_semaphore.release();
    	 System.out.println("Baker eats his sandwich");
     }
 }

}
