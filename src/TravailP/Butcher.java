package TravailP;

/**
 * 
 * The Butcher class for the Sandwich Gourmet Problem.
 * This class will 
 * Concurrent Programming - TP6
 * 
 * @author      Roberto Ramos Chavez <roberto.ramoschavez@edu.hefr.ch>
 * @version     1.9              
 * @since       2012-04-20
 *
 *
 *
 */

public class Butcher implements Runnable {

 @Override
 public void run() {
     while (true) {
    	 try {
			SandwichGourmets.mutexHam.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	 System.out.println("Butcher adds ham and makes sandwich");
    	 //Clean the table after eating
    	 SandwichGourmets.isBread	= false;
    	 SandwichGourmets.isButter	= false;
    	 SandwichGourmets.isHam		= false;
    	 SandwichGourmets.grocer_semaphore.release();
    	 System.out.println("Bucher eats his sandwich");
     }
 }
}